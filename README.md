# JFINAL-NAT简介

JFINAL-NAT 是基于 jfinal 与 httpclient 实现的内网穿透反向代理插件

备注 : 基于数字证书/权限的反向代理 

原理: HTTP头中加入: Cookie: iPlantDirectoryPro=<安全平台认证令牌>

# 快速入门

**1. 引入 [jfinalnat.jar](http://47.94.91.75:8080/jfinalnat-1.0.jar) 包, 或引入maven配置**
```xml
<dependency>
    <groupId>com.gitee.sohnny</groupId>
    <artifactId>jfinalnat</artifactId>
    <version>1.1</version>
</dependency>
```

**2. 新建 reverse_proxy.properties 配置文件 内容如下:**
```txt
    ##################  HttpClient start ##################
    maxTotal = 200
    defaultMaxPerRoute = 20
    localhostRoutePort = 80
    maxPerRoute = 50
    targetUrl = http://127.0.0.1:8080/testService
    ##################  HttpClient end ##################
    ##################  reverse_proxy start ##################
    isEnable = false
    amHost = cert.test.com
    updateTime = 600
    updateFastTime = 1
    keyStorePath = c:/test.keystore
    password = 123456
    targetUrl_cross = http://cert.test.com:9000/mapping/test
    ##################  reverse_proxy end ###################
```
```txt
	maxTotal 最大http线程池为 200;
	defaultMaxPerRoute 默认的路由数为 20;
	targetUrl 开发时反向代理的目标服务器地址;
	isEnable 为false时的请求转向地址targetUrl,为true是转向targetUrl_cross
	amHost 获取token的主机
	updateTime 获取token的频率为 600秒
	updateFastTime 获取token的最快频率 1秒
	keyStorePath 打包好的keystore证书文件
	password keystore文件的密码
```
**3.  对 jfinal 的 JFinalConfig 配置如下:**
```java
	@Override
	public void afterJFinalStart() {
		PoolingHttpClientConfig.init();
		System.err.println("httpClient代理配置完毕");
	}
```
# 用法
```java
import com.jfinal.core.Controller;
import com.jfinalnat.httpclient.annotation.Proxy;
import com.jfinalnat.httpclient.sdk.HttpService;

public class UserAction extends Controller {

	@Proxy(uri="/getUser") //代理目标服务器的地址为 (targetUrl_cross/targetUrl) + /getAuth
	public void test() {};
	
	/**
	 * 主界面
	 */
	public void index() { //没有Proxy注解则不走代理, 直接进入方法体, 在方法体内可通过HttpService.get等方法进行服务接口调用
		User user = HttpService.get(User.class, "/getUser");
		setAttr("user", user);
		render("index.html");
	}
	
	/**
	 * 绑定
	 */
	@Proxy //无参的默认为请求代理服务器的uri, (targetUrl_cross/targetUrl) + uri
	public void bind() {}
```
# 项目maven依赖
```xml
<dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
    
   	<dependency>
		<groupId>org.eclipse.jetty</groupId>
		<artifactId>jetty-jsp</artifactId>
		<version>8.1.8.v20121106</version>
		<scope>provided</scope>
	</dependency>
    <dependency>
    	<groupId>com.jfinal</groupId>
    	<artifactId>jfinal</artifactId>
    	<version>3.3</version>
    	<scope>provided</scope>
    </dependency>
    <dependency>
	    <groupId>org.apache.httpcomponents</groupId>
	    <artifactId>httpclient</artifactId>
	    <version>4.5.2</version>
	    <scope>provided</scope>
	</dependency>
	<!-- https://mvnrepository.com/artifact/org.apache.httpcomponents/httpmime -->
	<dependency>
	    <groupId>org.apache.httpcomponents</groupId>
	    <artifactId>httpmime</artifactId>
	    <version>4.5.2</version>
	    <scope>provided</scope>
	</dependency>
	<!-- https://mvnrepository.com/artifact/com.google.code.gson/gson -->
	<dependency>
	    <groupId>com.google.code.gson</groupId>
	    <artifactId>gson</artifactId>
	    <version>2.8.1</version>
	    <scope>provided</scope>
	</dependency>	
	<!-- https://mvnrepository.com/artifact/log4j/log4j -->
	<dependency>
	    <groupId>log4j</groupId>
	    <artifactId>log4j</artifactId>
	    <version>1.2.17</version>
	    <scope>provided</scope>
	</dependency>
	<!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-api -->
	<dependency>
	    <groupId>org.slf4j</groupId>
	    <artifactId>slf4j-api</artifactId>
	    <version>1.7.25</version>
	    <scope>provided</scope>
	</dependency>
	<!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-log4j12 -->
	<dependency>
	    <groupId>org.slf4j</groupId>
	    <artifactId>slf4j-log4j12</artifactId>
	    <version>1.7.25</version>
	    <scope>provided</scope>
	</dependency> 
  </dependencies>
  
   <build>
    <finalName>jfinalnat</finalName>
	    <plugins>
		  <plugin>  
			    <groupId>org.apache.maven.plugins</groupId>  
			    <artifactId>maven-war-plugin</artifactId>  
			    <version>3.2.0</version>  
		  </plugin> 
	      <plugin>
               <artifactId>maven-compiler-plugin</artifactId>                                                               
               <configuration>                   
                    <source>1.8</source>                                       
                    <target>1.8</target>
                    <encoding>UTF-8</encoding> 
                    <compilerArguments>            
                       <extdirs>src\main\webapp\WEB-INF\lib</extdirs> 
                    </compilerArguments>
               </configuration>
		  </plugin>
	  </plugins>
  </build>
```

**JFinal-nat**

