package com.jfinalnat.httpclient.annotation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RUNTIME)
@Documented
public @interface Proxy {
	/**
	 * 代理请求的URI
	 * @return
	 */
	String uri() default "";  
}
