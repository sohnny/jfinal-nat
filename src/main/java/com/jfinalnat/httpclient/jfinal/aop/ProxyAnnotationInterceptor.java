package com.jfinalnat.httpclient.jfinal.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.jfinalnat.httpclient.annotation.Proxy;
import com.jfinalnat.httpclient.sdk.DefaultHttpProxy;

public class ProxyAnnotationInterceptor implements Interceptor {
	
	private static final Log LOG = Log.getLog(Class.class);

	public void intercept(Invocation inv) {
		
		Proxy ma = inv.getMethod().getAnnotation(Proxy.class);
		
		if (ma != null) {
			
			String uri = ma.uri();
			Controller c = inv.getController();
			HttpServletRequest request = c.getRequest();
            HttpServletResponse response = c.getResponse();
            if (uri.equals("")) {
            	LOG.info("请求真实IP: "+getIpAddress(request));
            	new DefaultHttpProxy().execute(request, response);
            } else {            	
            	LOG.info("IP: "+getIpAddress(request)+"  代理:"+uri);
            	new DefaultHttpProxy().execute(request, response, uri);
            }
			c.renderNull();
			
			return;
			
		} else {
			
			inv.invoke();
			
		}
	}
	
    /** 
     * 获取用户真实IP地址，不使用request.getRemoteAddr();的原因是有可能用户使用了代理软件方式避免真实IP地址, 
*  
     * 可是，如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值，究竟哪个才是真正的用户端的真实IP呢？ 
     * 答案是取X-Forwarded-For中第一个非unknown的有效IP字符串。 
     *  
     * 如：X-Forwarded-For：192.168.1.110, 192.168.1.120, 192.168.1.130, 
     * 192.168.1.100 
     *  
     * 用户真实IP为： 192.168.1.110 
     *  
     * @param request 
     * @return 
     */  
    private String getIpAddress(HttpServletRequest request) {  
        String ip = request.getHeader("x-forwarded-for");  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
        }  
        return ip;  
    }

}
