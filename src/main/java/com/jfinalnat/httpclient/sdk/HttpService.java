package com.jfinalnat.httpclient.sdk;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.util.EntityUtils;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

public class HttpService {
	
	
   

	/**
	 * HttpClient Get请求
	 * @param clazz
	 * @param uri 请求相对路径
	 * @param keyValues 例如： name=张三
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T>T get(Class<?> clazz, String uri, String...keyValues) {
		
		CloseableHttpResponse response = null;

		uri = urlAddPara(uri, keyValues);
    	HttpGet httpGet = new HttpGet(PoolingHttpClientConfig.targetUrl+uri);
    	try {
			response = HttpClientFactory.getHttpClient().execute(httpGet, HttpClientContext.create());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	return new MyResponseHandler<T>((Class<T>) clazz).handleResponse(response);
    	
	}
	
	/**
	 * HttpClient Post 请求
	 * @param clazz
	 * @param uri
	 * @param nvps
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T>T post(Class<?> clazz, String uri, List<NameValuePair> nvps) {
		
		 CloseableHttpResponse response = null;
		try {

	    	HttpPost httpPost = new HttpPost(PoolingHttpClientConfig.targetUrl+uri);
	    	httpPost.setEntity(new UrlEncodedFormEntity(nvps));
			response = HttpClientFactory.getHttpClient().execute(httpPost, HttpClientContext.create());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	return new MyResponseHandler<T>((Class<T>) clazz).handleResponse(response);
    	
	}
	
	/**
	 * HttpClient Get请求
	 * @param clazz
	 * @param uri 请求相对路径
	 * @param keyValues 例如： name=张三
	 * @return json
	 */
	public static String get(String uri, String...keyValues) {
		
		 CloseableHttpResponse response = null;
		 String json = null;

		uri = urlAddPara(uri, keyValues);
    	HttpGet httpGet = new HttpGet(PoolingHttpClientConfig.targetUrl+uri);
		
    	try {
    		
			response = HttpClientFactory.getHttpClient().execute(httpGet, HttpClientContext.create());
			HttpEntity entity = response.getEntity();
			json =  entity != null ? EntityUtils.toString(entity) : null;
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	return json;
    	
	}
	
	/**
	 * HttpClient Post 请求
	 * @param clazz
	 * @param uri
	 * @param nvps
	 * @return
	 */
	public static String post(String uri, List<NameValuePair> nvps) {
		
		 CloseableHttpResponse response = null;
		 String json = null;
		try {

	    	HttpPost httpPost = new HttpPost(PoolingHttpClientConfig.targetUrl+uri);
	    	httpPost.setEntity(new UrlEncodedFormEntity(nvps));
	    	
			response = HttpClientFactory.getHttpClient().execute(httpPost, HttpClientContext.create());
			
			HttpEntity entity = response.getEntity();
			json =  entity != null ? EntityUtils.toString(entity) : null;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return json;
	}
	
	/**
	 * 拼接get请求参数
	 * @param uri
	 * @param keyValues
	 * @return
	 */
	private static String urlAddPara(String uri, String...keyValues) {
		
		if (keyValues.length == 0) return uri;
		
		if (uri.contains("?")) {
			for (int i=0; i < keyValues.length; i++) {
				uri += "&" + keyValues[i];
			}	
		} else {
			uri += "?" + keyValues[0];
			for (int i=1; i < keyValues.length; i++) {
				uri += "&" + keyValues[i];
			}
		}
		
		return uri;
	}
	
	public static void main(String[] args) throws ClientProtocolException, IOException {
		PoolingHttpClientConfig.init();
		Page<Record> page = HttpService.get(Page.class, "/test");
		System.out.println(page.getList());
	}
	
}
