package com.jfinalnat.httpclient.sdk;

import org.apache.log4j.Logger;

import com.chunyv.maas.pojo.token.TokenTimerHandler;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinalnat.token.factory.TokenTimerHandlerFactory;

public class PoolingHttpClientConfig {

	private static final Logger LOG = Logger.getLogger(Class.class);
	
	public static String targetUrl = "127.0.0.1:80/projectName";
	
	// Increase max total connection to 200
	public static Integer maxTotal = 200;
	// Increase default max connection per route to 20
	public static Integer defaultMaxPerRoute = 20;
	
	public static Integer localhostRoutePort = 80;
	// Increase max connections for localhost:80 to 50
	public static Integer maxPerRoute = 50;
	
	public static String local_cookie_path = "/";
	
	
	/**
	 * SSL连接 配置
	 */
	public static String amHost;
	public static String password;
	public static String keyStorePath;
	public static Long updateTime = 600L;
	public static Long updateFastTime = 1L;

	public static Boolean isEnable = true;

	public static Boolean isEnableConnectionManager = false;
	
	public static void init () {
		
		Prop prop = PropKit.use("reverse_proxy.properties");
		
		if (prop.get("isEnable") != null) {
			isEnable  = prop.getBoolean("isEnable");
		}
		
		if (prop.get("isEnableConnectionManager") != null) {
			isEnableConnectionManager  = prop.getBoolean("isEnableConnectionManager");
		}
		
		if (prop.get("targetUrl") != null) {
			if (isEnable) {
				targetUrl = prop.get("targetUrl_cross");
			} else {
				targetUrl = prop.get("targetUrl");
			}
		} else {
			LOG.error("SSL --- httpClient 请求转发 targetUrl 为空");
		}
		if (prop.get("maxTotal") != null) {
			maxTotal = prop.getInt("maxTotal");
		} 
		if (prop.get("defaultMaxPerRoute") != null) {
			defaultMaxPerRoute = prop.getInt("defaultMaxPerRoute");
		} 
		if (prop.get("localhostRoutePort") != null) {
			localhostRoutePort = prop.getInt("localhostRoutePort");
		} 
		if (prop.get("maxPerRoute") != null) {
			maxPerRoute = prop.getInt("maxPerRoute");
		} 
		
		if (prop.get("amHost") != null) {
			amHost = prop.get("amHost");
		} else {
			LOG.error("SSL --- amHost 为空");
		}
		if (prop.get("password") != null) {
			password = prop.get("password");
		} else {
			LOG.error("SSL --- password 为空");
		}
		if (prop.get("keyStorePath") != null) {
			keyStorePath = prop.get("keyStorePath");
		} else {
			LOG.error("SSL --- keyStorePath 为空");
		}
		if (prop.get("updateTime") != null) {
			updateTime = prop.getLong("updateTime");
		} else {
			LOG.info("SSL --- updateTime 为空");
		}
		if (prop.get("updateFastTime") != null) {
			updateFastTime = prop.getLong("updateFastTime");
		} else {
			LOG.info("SSL --- updateFastTime 为空");
		} 
		if (prop.get("local_cookie_path") != null) {
			local_cookie_path = prop.get("local_cookie_path");
		}
		
		//初始化 httpClient
		if (isEnable) {
			TokenTimerHandler  tokenTimerHandler = TokenTimerHandlerFactory.createSSLSocketFactoryImpl(amHost, updateTime, updateFastTime, keyStorePath, password);
			System.out.println("token获取结果: "+tokenTimerHandler.getTokenURLEncode());
		}
	}
	
}
