package com.jfinalnat.token.factory;

import com.chunyv.maas.pojo.ws.handler.HandlerResolverImpl;
import com.jfinal.kit.PropKit;

public class HandlerResolverFactory {
	
	private static HandlerResolverImpl handlerResolverImpl;
	
	public static HandlerResolverImpl createHandlerResolverImpl () {
		
		if (handlerResolverImpl == null) {
			//读取数据库配置文件
			PropKit.use("config.properties");
			handlerResolverImpl = new HandlerResolverImpl();
			handlerResolverImpl.setTokenHandler(TokenTimerHandlerFactory.createSSLSocketFactoryImpl(PropKit.get("amHost"), PropKit.getLong("updateTime", 600l), PropKit.getLong("updateFastTime", 1l), PropKit.get("keyStorePath"), PropKit.get("password")));
		}		
		return handlerResolverImpl;
	}
	
}
