package com.jfinalnat.token.factory;

import java.io.File;

import com.chunyv.maas.pojo.ssl.factory.SSLSocketFactoryImpl;

public class SSLSocketFactory {

	private static SSLSocketFactoryImpl sslSocketFactoryImpl;
	
	public static SSLSocketFactoryImpl createSSLSocketFactoryImpl (String keyStorePath, String password) {
		
		if (sslSocketFactoryImpl == null) {
			
			sslSocketFactoryImpl =  new SSLSocketFactoryImpl();
			sslSocketFactoryImpl.setCertFile(new File(keyStorePath));
			sslSocketFactoryImpl.setCertPassWord(password);
			
			//sslSocketFactoryImpl.sethandshakeCompletedListenerImpl(handshakeCompletedListenerImpl); // 可选
			
			sslSocketFactoryImpl.init();
		}		
		
		return sslSocketFactoryImpl;
		
	}

}
