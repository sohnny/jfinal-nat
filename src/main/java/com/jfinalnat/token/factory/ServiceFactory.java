package com.jfinalnat.token.factory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 
 * WebService 服务类工厂
 * 
 * 用于创建出WebService服务代理对象, 注入WebService拦截器对象(用于内外网穿越)
 * 
 * example: WebServiceImpl wsImpl = ServiceFactory.createService(WebServiceImplService.class);
 * 
 * @author Sohnny
 *
 */
public class ServiceFactory {
	
	@SuppressWarnings("unchecked")
	public static <T> T getInstance(Class<?> clazz) {
		
		Object nobj = null;
		
		try {
			
			Object obj = clazz.newInstance();
			
			//Method method = clazz.getMethod("setHandlerResolver", HandlerResolver.class);
			//method.invoke(obj, HandlerResolverFactory.createHandlerResolverImpl());
			
			String className = clazz.getSimpleName();
			String methodName = "get"+className.substring(0, className.length()-7)+"Port";
			Method methodPort = clazz.getMethod(methodName);
			nobj = methodPort.invoke(obj);
			
		} catch (NoSuchMethodException e1) {
			e1.printStackTrace();
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (InstantiationException e2) {
			e2.printStackTrace();
		} catch (IllegalAccessException e2) {
			e2.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return (T) nobj;
	}
	
}
