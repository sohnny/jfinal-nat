package com.jfinalnat.token.factory;

import java.net.MalformedURLException;

import com.chunyv.maas.pojo.token.TokenHandler;

public class TokenHandlerFactory {

	private static TokenHandler tokenHandler;
	
	public static TokenHandler createTokenHandler (String amHost ,String keyStorePath, String password) {
		
		if (tokenHandler == null) {
			
			tokenHandler = new TokenHandler();
			try {
				tokenHandler.setAmHost(amHost);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			tokenHandler.setMsf(SSLSocketFactory.createSSLSocketFactoryImpl(keyStorePath, password));
			
		}		
		
		return tokenHandler;
		
	}
	
}
