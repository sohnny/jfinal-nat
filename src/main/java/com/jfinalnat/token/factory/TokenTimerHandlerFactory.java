package com.jfinalnat.token.factory;

import com.chunyv.maas.pojo.token.TokenTimerHandler;

public class TokenTimerHandlerFactory {

	private static TokenTimerHandler tokenTimerHandler;
	
	public static TokenTimerHandler createSSLSocketFactoryImpl (String amHost, Long updateTime, Long updateFastTime, String keyStorePath, String password) {
		
		if (tokenTimerHandler == null) {
			
			tokenTimerHandler = new TokenTimerHandler();
			tokenTimerHandler.setTokenHandler(TokenHandlerFactory.createTokenHandler(amHost, keyStorePath, password));
			tokenTimerHandler.setTokenUpdateTime(updateTime);
			tokenTimerHandler.setTokenUpdateFastTime(updateFastTime);
			tokenTimerHandler.init();
			
		}		
		
		return tokenTimerHandler;
		
	}
	
}
